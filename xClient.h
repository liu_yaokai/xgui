//
// Created by "Yaokai Liu" on 11/29/22.
//

#ifndef XGUI_XCLIENT_H
#define XGUI_XCLIENT_H

#include "xtypes.h"

typedef struct {
    void * processor;
    void * drawer;
} xClient;

#endif //XGUI_XCLIENT_H
