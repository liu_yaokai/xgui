//
// Created by Yaokai Liu on 11/29/22.
//

#ifndef XGUI_XEVENT_H
#define XGUI_XEVENT_H


#include "xtypes.h"


typedef enum {
    handle = 1,
    focus = 2,
    pointer = 4,
    handle_focus = handle + focus,
    handle_pointer = handle + pointer,
    focus_pointer = focus + pointer,
} event_kind;


typedef enum{
    none
} event_type;


struct xEvent {
    event_kind  kind;
    event_type  type;
    xInt        signal;
    void *      reaction;
};


#endif //XGUI_XEVENT_H
